import ipywidgets as widgets
from traitlets import Unicode, Integer
import traitlets
from traittypes import Array
from ipyvolume.serialize import array_sequence_serialization, color_serialization
import pythreejs

@widgets.register
class DymMesh(pythreejs.Object3D):
    _model_name = Unicode('DymMeshModel').tag(sync=True)
    _model_module = Unicode('susivis').tag(sync=True)
    x = Array(default_value=None).tag(sync=True, **array_sequence_serialization)
    y = Array(default_value=None).tag(sync=True, **array_sequence_serialization)
    z = Array(default_value=None).tag(sync=True, **array_sequence_serialization)
    normals = Array(default_value=None, allow_none=True).tag(sync=True, **array_sequence_serialization)
    u = Array(default_value=None, allow_none=True).tag(sync=True, **array_sequence_serialization)
    v = Array(default_value=None, allow_none=True).tag(sync=True, **array_sequence_serialization)
    vert_visibility = traitlets.Union([Array(default_value=None, allow_none=True).tag(sync=True, **array_sequence_serialization),
                           traitlets.Float().tag(sync=True)],
                           default_value=1).tag(sync=True)
    triangles =  Array(default_value=None, allow_none=True).tag(sync=True, **array_sequence_serialization)
    
    #origin = traitlets.List(traitlets.CFloat, default_value=[0, 0, 0], minlen=3, maxlen=3).tag(sync=True)
    #domainscale = traitlets.CFloat(default_value=0).tag(sync=True)
    xbounds = traitlets.List(traitlets.CFloat, default_value=[0, 0], minlen=2, maxlen=2).tag(sync=True)
    ybounds = traitlets.List(traitlets.CFloat, default_value=[0, 0], minlen=2, maxlen=2).tag(sync=True)
    zbounds = traitlets.List(traitlets.CFloat, default_value=[0, 0], minlen=2, maxlen=2).tag(sync=True)

    smooth = traitlets.CBool(default_value=True).tag(sync=True);

#    selected = Array(default_value=None, allow_none=True).tag(sync=True, **array_sequence_serialization)
    sequence_index = Integer(default_value=0).tag(sync=True)
    color = Array(default_value="red", allow_none=True).tag(sync=True, **color_serialization)
#    color_selected = traitlets.Union([Array(default_value=None, allow_none=True).tag(sync=True, **color_serialization),
#                                     Unicode().tag(sync=True)],
#                                     default_value="green").tag(sync=True)
#    geo = traitlets.Unicode('diamond').tag(sync=True)
    a = traitlets.CFloat(default_value=1.0).tag(sync=True)
    visible = traitlets.CBool(default_value=True).tag(sync=True)
    visible_faces = traitlets.CBool(default_value=True).tag(sync=True)

    clipping = traitlets.CBool(default_value=False).tag(sync=True)
    clip_origin = traitlets.List(traitlets.CFloat, default_value=[0, 0, 0], minlen=3, maxlen=3).tag(sync=True)
    clip_normal = traitlets.List(traitlets.CFloat, default_value=[0, 0, 1], minlen=3, maxlen=3).tag(sync=True)

    side = traitlets.CaselessStrEnum(['front', 'back', 'both'], 'both').tag(sync=True)

@widgets.register
class Tool(pythreejs.Object3D):
    _model_name = Unicode('ToolModel').tag(sync=True)
    _model_module = Unicode('susivis').tag(sync=True)

    visible = traitlets.CBool(default_value=True).tag(sync=True)

@widgets.register
class Clipper(Tool):
    _model_name = Unicode('ClipperModel').tag(sync=True)
    _model_module = Unicode('susivis').tag(sync=True)
    
    clip_origin = traitlets.List(traitlets.CFloat, default_value=[0, 0, 0], minlen=3, maxlen=3).tag(sync=True)
    clip_normal = traitlets.List(traitlets.CFloat, default_value=[0, 0, 1], minlen=3, maxlen=3).tag(sync=True)

    color = traitlets.Union([traitlets.List(traitlets.CFloat, default_value=[1, 0, 0], minlen=3, maxlen=3).tag(sync=True),
                                     Unicode().tag(sync=True)],
                                     default_value="red").tag(sync=True)    