import numpy as np
import vtk
from skimage import measure

from vtk.util.numpy_support import numpy_to_vtk, vtk_to_numpy

def prepare_filter_array():
    pass

def normalize_data_to_color(data, cm):
    data = np.array(data)
    data_norm = data - data.min()
    data_norm = data_norm / data.ptp()
    color = cm(data_norm)    
    return color


def isosurfacevtk(volume, value):
    vtk_volume_array = numpy_to_vtk(volume.ravel(order='F'))

    vtk_volume_data = vtk.vtkImageData();
    vtk_volume_data.SetDimensions(volume.shape)
    vtk_volume_data.SetSpacing([1,1,1])
    vtk_volume_data.SetOrigin([0,0,0])
    vtk_volume_data.GetPointData().SetScalars(vtk_volume_array)

    contourFilter = vtk.vtkContourFilter();
    contourFilter.SetInputData(vtk_volume_data);
    contourFilter.GenerateValues(1, value, value); # (numContours, rangeStart, rangeEnd)
    contourFilter.Update();


    polydata = contourFilter.GetOutput();


    vertices = vtk_to_numpy(polydata.GetPoints().GetData())

    vx,vy,vz =  np.transpose(vertices)

    polys = vtk_to_numpy(polydata.GetPolys().GetData())
    polys = np.delete(polys, slice(None, None, 4))

    polys = polys.reshape((polys.shape[0]/3,3))

    normals = vtk_to_numpy(polydata.GetPointData().GetArray('Normals')).flatten()

    return vx,vy,vz, polys, normals

def isosurfacesk(volume,value):
    res = measure.marching_cubes_lewiner(volume, value)
    verts, polys = res[:2]
    vx, vy, vz = verts.T
    return vx,vy,vz,polys