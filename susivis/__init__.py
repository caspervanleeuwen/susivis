from ._version import version_info, __version__

from .susidraw import *
import ipyvolume
from .widgets import *

def _jupyter_nbextension_paths():
    return [{
        'section': 'notebook',
        'src': 'static',
        'dest': 'susivis',
        'require': 'susivis/extension'
    }]
