var susivis = require('./index');
var base = require('@jupyter-widgets/base');

module.exports = {
  id: 'susivis',
  requires: [base.IJupyterWidgetRegistry],
  activate: function(app, widgets) {
      widgets.registerWidget({
          name: 'susivis',
          version: susivis.version,
          exports: susivis
      });
  },
  autoStart: true
};

