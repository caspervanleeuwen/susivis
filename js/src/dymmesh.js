var _ = require('underscore')
var widgets = require('@jupyter-widgets/base');
var THREE = require('three')
var pythreejs = require('jupyter-threejs');
var ipv = require('ipyvolume')
//import * as values from './values'

var DymMeshModel = pythreejs.Object3DModel.extend({
    initialize: function() {
        pythreejs.Object3DModel.prototype.initialize.apply(this, arguments);
        this.initPromise.bind(this).then(this.initialize_dym_mesh_model);
    },
    initialize_dym_mesh_model: function() {
        this.renderer = null;
        this.mesh = null
        //this.mesh_back = null
        this.geometry_buffer = null

        this.domain_matrix = new THREE.Matrix4();

        var smooth = this.get('smooth');

        this.material_normal = new THREE.ShaderMaterial({
            uniforms: {
                domain_matrix : { type: "m4", value: this.domain_matrix},
                clipping : { type: "i", value: this.get('clipping')},
                clip_origin : { type: "v3", value: this.get('clip_origin')},
                clip_normal : { type: "v3", value: this.get('clip_normal')},
                a: { type: 'f', value: this.get('a')},
            },
            side:THREE.DoubleSide,
            transparent: true,
            depthWrite: true, 
            depthTest: true,
            blending: THREE.NormalBlending,
            vertexShader: require('raw-loader!../glsl/dym-mesh-vertex.glsl'),
            fragmentShader: (smooth && this.get('normals') != null ? "": "#define USE_FLAT\n")+require('raw-loader!../glsl/dym-mesh-fragment.glsl'),
            visible: this.get("visible") && this.get("visible_faces")
                })
        this.material_normal.extensions = {derivatives: true}
        this.material_rgb = this.material_normal.clone()
        this.material_rgb.vertexShader = "#define USE_RGB\n"+this.material_rgb.vertexShader
        this.material_rgb.fragmentShader = "#define USE_RGB\n"+this.material_rgb.fragmentShader
        this.material_normal.needsUpdate = true;
        this.material_rgb.needsUpdate = true;
        this.create_mesh()
        this.on("change:sequence_index", this.on_sequence, this)
        this.on("change:color change:x change:y change:z change:v change:u change:vert_visibility",   this.on_change_geometry_attributes, this)
        this.on("change:triangles",   this.on_change_triangles, this)        
        this.on("change:clip_normal change:clip_origin change:clipping change:a", this.on_change_material_uniforms, this)
        this.on("change:visible change:visible_faces", this.update_visibility, this)
        this.on("change:side", this.update_side, this)
    },
    set_renderer: function (renderer) {
        this.renderer = renderer;
    },
    update_visibility: function () {
        this.material_normal.visible = this.get("visible") && this.get("visible_faces");
        this.material_rgb.visible = this.get("visible") && this.get("visible_faces");
        this.material_normal.needsUpdate = true;
        this.material_rgb.needsUpdate = true;
        this.trigger('need_render');
    },
    set_limits: function(limits) {
        var xptp = limits.xlim[1] - limits.xlim[0]
        var yptp = limits.ylim[1] - limits.ylim[0]
        var zptp = limits.zlim[1] - limits.zlim[0]

        var domainscale = Math.max(Math.max(xptp,yptp),zptp)

        this.domain_matrix.identity()
                                .multiply((new THREE.Matrix4()).makeTranslation(-0.5,-0.5,-0.5))
                                .multiply((new THREE.Matrix4()).makeScale(1/xptp,1/yptp,1/zptp))
                                .multiply((new THREE.Matrix4()).makeTranslation(-(limits.xlim[0]),-(limits.ylim[0]),-(limits.zlim[0])));     
        this.material_normal.uniforms.domain_matrix.value = this.domain_matrix;
        this.material_rgb.uniforms.domain_matrix.value = this.domain_matrix;
        this.material_normal.needsUpdate = true;                           
        this.material_rgb.needsUpdate = true;                           
    },
    on_change_geometry_attributes: function(m) {
        var sequence_index = this.get("sequence_index");
        _.each(m.changed, function(value, key){
            this.geometry_buffer.attributes[key].array.set(value[sequence_index]);
            this.geometry_buffer.attributes[key].needsUpdate = true;
        },this)
        this.update_()
    },
    on_change_material_uniforms: function(m) {
        _.each(m.changed, function(value, key){
            this.material_normal.uniforms[key].value = value;
            this.material_rgb.uniforms[key].value = value;
        },this)
        this.material_normal.needsUpdate = true;
        this.material_rgb.needsUpdate = true;
        this.update_()
    },
    on_change_triangles: function(m){
        var sequence_index = this.get("sequence_index");
        var trIdx = this.get('triangles')[sequence_index]
        this.geometry_buffer.index.array.set(trIdx);
        this.geometry_buffer.index.needsUpdate = true;
    },
    on_sequence: function(sequence){
        var sequence_index = this.get("sequence_index");


        var scalar_names = ['x', 'y', 'z', 'u', 'v', 'vert_visibility'];
        var vector4_names = ['color']

        var current  = new ipv.Values(scalar_names, [], _.bind(this.get_current, this), sequence_index, vector4_names)

        current.trim(current.length); // make sure all arrays are of equal length
        var current_length = current.length;

        current.merge_to_vec3(['x', 'y', 'z'], 'vertices')
        current.ensure_array(['color', 'vert_visibility'])

        var triangles = this.get('triangles')
        var normals = this.get('normals')

        if(triangles) {
            var trianglesFrame = triangles[sequence_index]
            this.geometry_buffer.attributes.position.array = current.array_vec3['vertices']
            this.geometry_buffer.attributes.color.array = current.array_vec4['color']
            this.geometry_buffer.attributes.vert_visibility.array = current.array['vert_visibility']
            this.geometry_buffer.index.array.set(trianglesFrame)
            if(normals != null) this.geometry_buffer.attributes.normal.array = normals[sequence_index];
            this.geometry_buffer.attributes.position.needsUpdate = true;
            this.geometry_buffer.attributes.color.needsUpdate = true;
            this.geometry_buffer.attributes.vert_visibility.needsUpdate = true;
            if(normals != null) this.geometry_buffer.attributes.normal.needsUpdate = true;
            this.geometry_buffer.index.needsUpdate = true;
        }
        this.trigger('need_render');
    },
    update_: function() {
        this.trigger('need_render');
    },
    _get_value: function(value, index, default_value) {
        var default_value = default_value;
        if(!value)
            return default_value
        // it is either an array of typed arrays, or a list of numbers coming from the javascript world
        if(_.isArray(value) && !_.isNumber(value[0]))
            return value[index % value.length]
        else
            return value
    },
    get_current: function(name, index, default_value) {
        return this._get_value(this.get(name), index, default_value)
    },
    _get_value_vec3: function(value, index, default_value) {
        var default_value = default_value;
        if(!value)
            return default_value
        if(_.isArray(value))
            return value[index % value.length]
        else
            return value
    },
    get_current_vec3: function(name, index, default_value) {
        return this._get_value_vec3(this.get(name), index, default_value)
    },
    create_mesh: function() {
        this.mesh = null
        //this.mesh_back = null
        this.geometry_buffer = null

        var sequence_index = this.get("sequence_index");


        var scalar_names = ['x', 'y', 'z', 'u', 'v', 'vert_visibility'];
        var vector4_names = ['color']

        var current  = new ipv.Values(scalar_names, [], _.bind(this.get_current, this), sequence_index, vector4_names)

        current.trim(current.length); // make sure all arrays are of equal length
        var current_length = current.length;

        current.merge_to_vec3(['x', 'y', 'z'], 'vertices')
        current.ensure_array(['color', 'vert_visibility'])

        var triangles = this.get('triangles')
        var normals = this.get('normals')
        if(triangles) {
            var trianglesFrame = triangles[sequence_index]
            this.geometry_buffer = new THREE.BufferGeometry();
            this.geometry_buffer.addAttribute('position', new THREE.BufferAttribute(current.array_vec3['vertices'], 3))
            this.geometry_buffer.addAttribute('color', new THREE.BufferAttribute(current.array_vec4['color'], 4))
            this.geometry_buffer.addAttribute('vert_visibility', new THREE.BufferAttribute(current.array['vert_visibility'], 1))
            this.geometry_buffer.setIndex(new THREE.BufferAttribute(trianglesFrame, 1))

            //this.geometry_buffer.computeVertexNormals();    
            if(normals != null) this.geometry_buffer.addAttribute('normal', new THREE.BufferAttribute(normals[sequence_index],3))

            this.mesh = new THREE.Mesh(this.geometry_buffer, this.material_normal);

            // BUG? because of our custom shader threejs thinks our object if out
            // of the frustum
            this.mesh.frustumCulled = false;
            //this.mesh_back.frustumCulled = false;
            this.mesh.material_normal = this.material_normal;
            this.mesh.material_rgb = this.material_rgb;
            this.obj.add(this.mesh);
        }
    },
    defaults: function() {
        return _.extend(widgets.WidgetModel.prototype.defaults(), {
            _model_name : 'DymMeshModel',
            _model_module : 'susivis',
            color: "red",
            vert_visibility: 1,
            smooth: true,
            sequence_index: 0,
            visible: true,
            visible_faces: true,
            clipping: false,
            clip_normal: [0.0,0.0,1.0],
            clip_origin: [0.0,0.0,0.0],
            a: 1.0
        })
    }}, {
    serializers: _.extend({
        x: ipv.array_or_json,
        y: ipv.array_or_json,
        z: ipv.array_or_json,
        u: ipv.array_or_json,
        v: ipv.array_or_json,
        normals: ipv.array_or_json,
        triangles: ipv.array_or_json,
        color: ipv.color_or_json,
        vert_visibility: ipv.array_or_json,
    }, widgets.WidgetModel.serializers)
});

module.exports = {
    DymMeshModel:DymMeshModel
}
