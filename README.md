# SuSiVis

SuSiVis, Suspension Simulation Visualization, contains wrapper functions around the Jupyter visualization package IPyVolume functionality that prepares the experiment data of the python package SuSiPop. This will separate the source without having to edit the pylab module of ipyvolume and will only show the need functionality to the user using SuSiVis in a Jupyter notebook.

# Installation

For installation (requires npm):
```
git clone https://gitlab.com/caspervanleeuwen/susivis.git
cd susivis
pip install -e . --process-dependency-links
jupyter nbextension install --py --symlink --sys-prefix susivis
jupyter nbextension enable --py --sys-prefix susivis
```